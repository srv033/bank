import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { FormControl } from '@angular/forms';
import { TransactionService } from '../transaction.service';
import { userAccount } from '../Account';
import { formatDate } from '@angular/common';
import { Transaction } from 'Transaction';

@Component({
  selector: 'app-view-account-statement',
  templateUrl: './view-account-statement.component.html',
  styleUrls: ['./view-account-statement.component.css']
})
export class ViewAccountStatementComponent implements OnInit {
  accList: userAccount[]=[];
  viewList: Transaction[]=[];
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
   displayedColumns: string[] = ['transaction_Id', 'from_Account', 'to_Account', 'amount','transaction_Time','account_Balance','status','remarks','flag','s_No'];

  dataSource = new MatTableDataSource<Transaction>(this.viewList);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

constructor(private transactionService:TransactionService){}

  ngOnInit() {
    this.transactionService.getAccList().subscribe(data=>{for(let i=0;i<data.length;i++){
      this.accList[i]=data[i];
    }},error=>console.log("Error Error..."),()=>console.log('done'));
    this.dataSource.paginator = this.paginator;

  }

  handleSubmit({value}){
    console.log(value.selectedAccount)
    const selectedStartDate= new Date(value.selectedStartDate);
    let startdate = JSON.stringify(selectedStartDate)
    console.log(startdate.slice(1,11))
    const myselectedEndDate= new Date(value.selectedEndDate);
    let date = JSON.stringify(myselectedEndDate)
    console.log(date.slice(1,11))
    console.log(this.viewList)
    this.transactionService.getViewStatement(value.selectedAccount,startdate.slice(1,11),date.slice(1,11)).subscribe(data=>{for(let i=0;i<data.length;i++){
      this.viewList[i]=data[i];
    }},error=>console.log("Error Error..."),()=>console.log('done'));
    this.dataSource = new MatTableDataSource<Transaction>(this.viewList);



    // const testDate= formatDate(format,myDate,'en-US')
  }

}
export class DatepickerValueExample {
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
}
// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

//  [
//   {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
//   {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
//   {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
//   {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
//   {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
//   {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
//   {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
//   {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
//   {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
//   {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
//   {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
//   {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
//   {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
//   {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
//   {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
//   {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
//   {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
//   {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
//   {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
//   {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
// ];
