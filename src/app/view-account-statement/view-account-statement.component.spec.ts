import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAccountStatementComponent } from './view-account-statement.component';

describe('ViewAccountStatementComponent', () => {
  let component: ViewAccountStatementComponent;
  let fixture: ComponentFixture<ViewAccountStatementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAccountStatementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAccountStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
