import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { TransactionService } from '../transaction.service';
import { userAccount} from '../Account';
import { Transaction } from 'Transaction';
import { observable } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { Beneficiary } from '../beneficiary';
import { MyResponse } from './myResponse';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-transfer-funds',
  templateUrl: './transfer-funds.component.html',
  styleUrls: ['./transfer-funds.component.css']
})
export class TransferFundsComponent implements OnInit  {
  isLinear = true;
  firstFormGroup: FormGroup;
  dropdownMenuGroup:FormGroup;
  secondFormGroup: FormGroup;
  public transaction:Transaction;
  public  accList:userAccount[]=[];
  public  benList: Beneficiary[]=[];
  bal:userAccount;
  public message: MyResponse = { "status": "not done" };
  beneficiaryDetail:Beneficiary;
  constructor(private datepipe:DatePipe,private transactionService:TransactionService, private router:Router,private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.transactionService.getAccList().subscribe(data=>{for(let i=0;i<data.length;i++){
      this.accList[i]=data[i];
    }},error=>console.log("Error Error..."),()=>this.test());
   this.transactionService.getBeneficiary().subscribe(data=>{for(let i=0;i<data.length;i++){
    this.benList[i]=data[i];}});

  console.log('data in acc list '+this.accList);
   //this.transactionService.getAccList().subscribe(data=>console.log(data));
    //console.log('hello entering');
    //this.transaction =this.transactionService.getAllTransaction();

    this.firstFormGroup = this._formBuilder.group({
      transactionAmount:new FormControl('',[
        Validators.min(1),
        Validators.required,
        Validators.max(100000)
      ]),
      transactionRemark:new FormControl('Type Your Own',[
        Validators.maxLength(20)
      ]),
      selectedAccount:new FormControl('',[
        Validators.required,
      ]),
     
      selectedUser: new FormControl('',[
        Validators.required,
      ])
    });
    this.dropdownMenuGroup = this._formBuilder.group({
      dropDownMenuAcc:new FormControl('',[
        Validators.required,
      ]),
      dropDownMenuIFSC:new FormControl('',[
        Validators.required,

      ]),
      dropDownMenuName:new FormControl('',[
        Validators.required,
      ]),
    });

    // this.secondFormGroup = this._formBuilder.group({
    //   // secondCtrl: ['', Validators.required]
    // });
  }

  // Test(value){
  //   this.accList=value;
  //    console.log(this.accList);
  // }
  // Test1(value){
  //   this.bal=value;
  //    console.log(this.bal.account_Balance);
  // }
  //  balance(value){
  //   this.transactionService.getBal(value).subscribe(data=>console.log(data));


  // }
  // modelForm = new FormGroup({
  //   transactionAmount:new FormControl('',[
  //     Validators.min(5000),
  //     Validators.required,
  //     Validators.max(100000)
  //   ]),
  //   transactionRemark:new FormControl('',[
  //     Validators.maxLength(20)
  //   ]),
  //   selectedAccount:new FormControl(),
  //   selectedUser: new FormControl()
  // });
  
test(){
  console.log("done");
  console.log('data in acc list '+this.accList);
}

  get transactionamount(){
    return this.firstFormGroup.get('transactionAmount');
  }
  
  get transactionremark(){
    return this.firstFormGroup.get('transactionRemark');
  }

  get selectedAccount(){
    return this.firstFormGroup.get('selectedAccount');
  }

  get selectedUser(){
    return this.firstFormGroup.get('selectedUser');
  }

  get today(){
    let cusDate= this.datepipe.transform(new Date(),'yyyy-MM-dd HH:mm:ss');
    return cusDate;
  }

  // handleFormSubmit({value}){
  //   console.log(value);
  //   const transaction = new Transaction();
  //   transaction.transactionId = "DBS"+Math.ceil(Math.random() * 100);
  //   transaction.fromAccount=value.selectedAccount;
  //   transaction.toAccount= value.selectedUser;
  //   transaction.amount=value.transactionAmount;
  //   transaction.remarks=value.transactionRemark;
  //   transaction.transactionTime=new Date();
  //   // course.desc= value.courseDesc;
  //   // course.name= value.courseName;
  //   // course.offer = value.offer;
  //   // course.price = value.coursePrice;
  //   // course.students = 0;
  //   // course.rating = 0;
  //   this.transactionService.addTransaction(transaction);
  //   this.router.navigate(['transferfundconfirmation'])
  // }

  gettransaction_Id(){
    return Math.floor(Math.random() * (+10000000 - +1000000)) + +1000000;
  }



  handleMatFormSubmit({value}){

    this.transaction = { transaction_Id: this.gettransaction_Id() , from_Account: value.selectedAccount, 
      to_Account: value.selectedUser, amount:value.transactionAmount, 	transaction_Time:this.today,
    account_Balance:0,status:1,remarks:value.transactionRemark,flag:0,s_No:1};
    console.log(this.transaction);
    this.transactionService.saveTransaction(this.transaction).subscribe(data=>data=this.transaction);
  }
  

  handleDropDownMenuFormSubmit({value}){
    console.log(value);
    // console.log(value.dropDownMenuName);
    this.beneficiaryDetail = { account_Id: value.dropDownMenuAcc, name: value.dropDownMenuName, ifsc_code: value.dropDownMenuIFSC, user_Id: 2001 };
    this.transactionService.saveBeneficiary(this.beneficiaryDetail).subscribe(data=>data=this.beneficiaryDetail);

    // this.transactionService.saveBeneficiary(this.beneficiaryDetail).subscribe(data => this.message = data,error=>console.log(error),()=>this.handleMessage());
  }
  handleMessage() {
    this.transactionService.add(this.message);
    console.log(this.message.status);
    this.router.navigate(['/message']);
  }
    
}
