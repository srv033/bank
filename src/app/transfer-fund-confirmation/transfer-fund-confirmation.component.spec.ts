import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferFundConfirmationComponent } from './transfer-fund-confirmation.component';

describe('TransferFundConfirmationComponent', () => {
  let component: TransferFundConfirmationComponent;
  let fixture: ComponentFixture<TransferFundConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferFundConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferFundConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
