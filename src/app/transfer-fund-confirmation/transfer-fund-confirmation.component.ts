import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../transaction.service';
import { Transaction } from 'Transaction';

@Component({
  selector: 'app-transfer-fund-confirmation',
  templateUrl: './transfer-fund-confirmation.component.html',
  styleUrls: ['./transfer-fund-confirmation.component.css']
})
export class TransferFundConfirmationComponent implements OnInit {
  transaction:Transaction[];
  constructor(private transactionService:TransactionService) { }

  ngOnInit() {
    // this.transaction =this.transactionService.getAllTransaction();
  }
}
