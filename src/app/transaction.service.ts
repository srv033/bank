import { Injectable } from '@angular/core';
import { Transaction } from 'Transaction';
import { userAccount } from './Account';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Beneficiary } from './beneficiary';
import { MyResponse } from './transfer-funds/myResponse';

//const configUrl = '';



@Injectable({
  providedIn: 'root'
})
@Injectable()
export class TransactionService {

  // transactions:Transaction[]
  // =[
  //   {
  //     fromAccount:"DBS00001",
  //     fromName:"Akash",
  //     fromBranch:"",
  //     fromIFSCCode:"string",
  //     amount:29829,
  //     remarks:"hehe ",
  //     balance:300000,
  //     toAccount:"DBS00002",
  //     toName:"string",
  //     toBranch:"string",
  //     toIFSCCode:"string",
  //     transactionTime:new Date(),
  //     status:true,
  //     transactionId:"string"
  //   },
  //   {
  //     fromAccount:"DBS00001",
  //     fromName:"Akash",
  //     fromBranch:"",
  //     fromIFSCCode:"string",
  //     amount:20000,
  //     remarks:"hehe ",
  //     balance:30000,
  //     toAccount:"DBS00003",
  //     toName:"string",
  //     toBranch:"string",
  //     toIFSCCode:"string",
  //     transactionTime:new Date(),
  //     status:true,
  //     transactionId:"string"
  //   }
  // ];
  public message: string;

  userAccounts:userAccount[];
  constructor(private http: HttpClient) { 
  

    
  // this.userAccounts=[
  //   {
  //     name:"Akash",
  //     accountNumber:"DBS00001",
  //     branch:"GB",
  //     phoneNumber:9494949492,
  //     address:"High Street Bombay",
  //     balance:20000,
  //     branchIFSC:"GBDBS001"
  //   },
  //   {
  //     name:"Ankitha",
  //     accountNumber:"DBS00002",
  //     branch:"GB",
  //     phoneNumber:949494532,
  //     address:"Peek Street Banglore",
  //     balance:30000,
  //     branchIFSC:"GBDBS001"
  //   },
  //   {
  //     name:"Elena Gilbert",
  //     accountNumber:"DBS00003",
  //     branch:"GB",
  //     phoneNumber:9494933332,
  //     address:"Sea Street Singapore",
  //     balance:10000,
  //     branchIFSC:"GBDBS001"
  //   }
  // ]
  }
  // getAllUsers = ():userAccount[]=>{
  //     return this.userAccounts;
  // }
  // getAllTransaction = ():Transaction[]=>{
  //   return this.transactions;
  // }

  
  // addTransaction(transaction:Transaction):Transaction[]{
  //   this.transactions.push(transaction);
  //   return this.transactions;
  // }
  private base_url="http://localhost:8080/";


  public getAccList(): Observable<userAccount[]>{
    return this.http.get<userAccount[]>(this.base_url+'customer/account/2003')
  }
  public getBal(value): Observable<userAccount>{
    return this.http.get<userAccount>(this.base_url+'account/'+value)
  }
  public getBeneficiary(): Observable<Beneficiary[]>{
    return this.http.get<Beneficiary[]>(this.base_url+'customer/getbeneficiar/2001')
  }

  public getViewStatement(accId,start,end): Observable<Transaction[]>{
    return this.http.get<Transaction[]>(this.base_url+'customer/account/viewstatement/'+accId+"/"+start+"/"+end);
  }
  saveBeneficiary(beneficiaryModel: Beneficiary):Observable<Beneficiary> {
    console.log('saved Beneficiary');
    return this.http.post<Beneficiary>(this.base_url+'customer/addbeneficiar/',beneficiaryModel)
  }

  saveTransaction(TransactionModel: Transaction):Observable<Transaction> {
    console.log('saved  Transaction');
    return this.http.post<Transaction>(this.base_url+'customer/transaction/',TransactionModel)
  }


  // find(empId: number): Observable<Emp> {
  //   return this.http.get<Emp>(this.base_url+'emp/find/'+empId);
  // }
  public add(message: MyResponse) {
    this.message = message.status;
    //console.log(JSON.stringify(message));
  }


  //handleAccForm({value}){
  //   console.log('entering')
  //   console.log(value);
  //   this.transactionService.getViewStatement().subscribe(data=>{for(let i=0;i<data.length;i++){
  //     this.viewStatementList[i]=data[i];}});
  // }


}