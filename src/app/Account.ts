export class userAccount{
    account_Id:number;
    branch:string;
    customer_Id:number;
    account_Balance:number;
    account_Type:string;
    opened_Date:Date;
    status:number;
}