import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CustomerSidebarComponent } from './customer-sidebar/customer-sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TransferFundsComponent } from './transfer-funds/transfer-funds.component';
import { ViewAccountStatementComponent } from './view-account-statement/view-account-statement.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';
import { SidebarProfileComponent } from './sidebar-profile/sidebar-profile.component';
import { CustomerHomeComponent } from './customer-home/customer-home.component';
import { TransferFundConfirmationComponent } from './transfer-fund-confirmation/transfer-fund-confirmation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { HttpClientModule } from '@angular/common/http';

import { 
  MatToolbarModule, 
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule ,
  MatStepperModule,
  MatInputModule,
  MatSelectModule,
  MatTreeModule,
  MatTableModule,
  MatAutocompleteModule,
  MatPaginatorModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatMenuModule,
} from '@angular/material';
import { NgLocalization, DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    CustomerSidebarComponent,
    NavbarComponent,
    TransferFundsComponent,
    ViewAccountStatementComponent,
    NotFoundComponent,
    SidebarProfileComponent,
    CustomerHomeComponent,
    TransferFundConfirmationComponent 
    ],
  imports: [
    BrowserModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatToolbarModule, 
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule ,
    MatStepperModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTreeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    HttpClientModule,

    RouterModule.forRoot([
      {
        path:'',
        redirectTo:'home',
        pathMatch:'full'
      },
      {
        path:'transferfunds',
        component:TransferFundsComponent
      },
      {
        path:'viewstatement',
        component:ViewAccountStatementComponent
      },
      {
        path:'transferfundconfirmation',
        component:TransferFundConfirmationComponent
      },

      {
        path:'**',
        component:NotFoundComponent
      }
  ]),
    BrowserAnimationsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
