export class Transaction{
    transaction_Id:number;
	from_Account:number;
	to_Account:number;
	amount:number;
	transaction_Time:string;
	account_Balance:number;
	status:number;
	remarks:string;
	flag:number;
	s_No:number;
}